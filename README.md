ZAP/OSADA
=========

This repository contains the working code for the Zap and Osada social network servers. The type of server you wish to run is chosen during site installation. 

Zap
===

Zap is a full featured social network application running under the Zot6 protocol. It provides enhanced privacy modes and identity/content mirroring across multiple servers ("nomadic identity"). It does not "federate" with non-nomadic servers, protocols, or projects. 


Osada
=====

Osada is a full featured social network application running under the ActivityPub protocol. It also communicates with and inter-operates with servers on the Zot6 network (such as Zap). Due to limitations in the ActivityPub protocol, Osada does **not** offer nomadic identity or enhanced privacy modes. 



